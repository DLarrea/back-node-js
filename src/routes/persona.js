const express = require('express');
var cors = require('cors');
const router = express.Router();

const { Pool, Client } = require('pg');
const connectionString = "postgres://ahcbhetk:VoUKxBzeuyGOtwiCoy_Ohf50fQsYoDoX@rajje.db.elephantsql.com:5432/ahcbhetk";
//const connectionString = "postgresql://postgres:postgres@localhost:5432/back";

const postgresConnection = new Client({
    connectionString: connectionString
});

postgresConnection.connect();

router.use(cors());

// GET all Personas
router.get('/persona', (req, response) => {
    postgresConnection.query('SELECT * FROM persona', (err, res) => {
        if (!err) {
            response.json(res.rows);
        } else {
            response.status(500).send({ mensaje: err.message, codigo: 500 });
            //console.log(err);
        }
    });
});

module.exports = router;